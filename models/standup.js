const mongoose = require("mongoose");

const requiredStringValidator = [
  function (value) {
    let testValue = value.trim();
    return testValue.length > 0;
  },
  // Custom Error text :
  "please supply a value for {PATH}",
];

const standUpSchema = new mongoose.Schema({
  teamMemberId: {
    type: mongoose.Schema.Types.ObjectId, //
    ref: "teamMembers", // reference to teamMember schema model
  },
  teamMember: {
    type: String,
    // required: true,
    // validate: requiredStringValidator,
  },
  project: {
    type: String,
    // required: true,
    // validate: requiredStringValidator,
  },
  workYesterday: {
    type: String,
    // required: true,
    // validate: requiredStringValidator,
  },
  workToday: {
    type: String,
    // required: true,
    // validate: requiredStringValidator,
  },
  impediment: {
    type: String,
    // required: true,
    default: "none",
  },
  createdOn: { type: Date, default: Date.now },
});

module.exports = mongoose.model("Standup", standUpSchema);
