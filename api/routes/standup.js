const Standup = require("../../models/standup");
const mongoose = require("mongoose");

module.exports = function (router) {
  // GET: the 12 newest stand-up meeting notes
  router.get("/standup", (req, res) => {
    Standup.find()
      .sort({ createdOn: 1 }) //sorting asignding
      .exec() // return a promiss
      .then((result) => res.status(200).json(result))
      .catch((err) =>
        res
          .status(500)
          .json({ message: "Error Finding StandUp Meeting Notes", error: err })
      );
  });

  // GET: by team member ID:
  router.get("/standup/:teamMemberId", function (req, res) {
    const query = {
      _teamMemberId: mongoose.Types.ObjectId(req.params.teamMemberId),
    };
    Standup.find(query)
      .sort({ createdOn: 1 })
      .exec()
      .then((result) => res.status(200).json(result))
      .catch((err) =>
        res.status(500).json({
          message: "Error finding standUps notes for team member Id",
          error: err,
        })
      );
  });

  // POST: Get New Meeting note document:
  router.post("/standup", function (req, res) {
    let note = new Standup(req.body);
    note.save((err, noteResponse) => {
      if (err) {
        return res.status(400).json(err);
      }
      res.status(200).json(noteResponse);
    });
  });
};
