const TeamMember = require("../../models/teamMember");

module.exports = function (router) {
  // GET: List of team members
  router.get("/team", (req, res) => {
    TeamMember.find()
      .sort({ name: 1 })
      .exec()
      .then((result) => res.status(200).json(result))
      .catch((err) =>
        res.status(500).json({
          message: "Error Finding Team Members",
          error: err,
        })
      );
  });

  // POST: Create New Project ...
  router.post("/team", function (req, res) {
    let member = new TeamMember(req.body);
    member.save((err, memberResponse) => {
      if (err) {
        return res.status(400).json(err);
      }
      res.json(200).json(memberResponse);
    });
  });
};
