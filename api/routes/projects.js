const Project = require("../../models/project");

module.exports = function (router) {
  // GET: List of active projects
  const query = {
    isActive: { $eq: true }, // $eq is operator means equal to .. so we can filter by
  };
  router.get("/projects", (req, res) => {
    Project.find(query)
      .sort({ name: 1 })
      .exec()
      .then((result) => res.status(200).json(result))
      .catch((err) =>
        res.status(500).json({
          message: "Error Finding Active Projects",
          error: err,
        })
      );
  });

  // POST: Create New Project ...
  router.post("/projects", function (req, res) {
    let project = new Project(req.body);
    project.save((err, projectResponse) => {
      if (err) {
        return res.status(400).json(err);
      }
      res.json(200).json(projectResponse);
    });
  });
};
