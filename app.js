const express = require("express");
const app = express(); // new Instance of express app
const api = require("./api"); // The path for the API set up with folder
const morgan = require("morgan"); //logger
const bodyParser = require("body-parser");
const cors = require("cors");

app.set("port", process.env.PORT || 8081);

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));

app.use(cors());

app.use("/api", api);
app.use(express.static("static"));

app.use(morgan("dev"));

app.use((req, res) => {
  const err = new Error("Not Fount");
  err.status = 404;
  res.json(err);
});

// Add MongoDB connection ...
const mongoose = require("mongoose");
mongoose.connect("mongodb://localhost:27017/virtualstandups", {
  useNewUrlParser: true,
  useUnifiedTopology: true,
});

// Reference to the mongoose connection ...
const db = mongoose.connection;
// Helpful to see if there is any error connecting to db throw mongoose ...
db.on("error", console.error.bind(console, "connection error:"));
db.once("open", () => {
  console.log("Connected to mongoDB");
  app.listen(app.get("port"), () => {
    console.log("API server is running on port " + app.get("port"));
  });
});
